export interface Image {
  id: number;
  title: string;
  image: string;
  description: string;
  active: boolean;
}
