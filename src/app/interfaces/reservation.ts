export interface Reservation {
  id: number;
  user_id: number;
  package_id: number;
  status: number;
  date: string;
}
