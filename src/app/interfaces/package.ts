export interface Package {
  id: number;
  title: string;
  slug: string;
  intro: string;
  description: string;
  price: number;
  image: string;
  active: boolean;
}
