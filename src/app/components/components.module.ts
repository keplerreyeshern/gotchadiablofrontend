import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarPublicComponent } from './public/navbar/navbar.component';
import { FooterPublicComponent } from './public/footer/footer.component';
import { NavbarAdminComponent } from './admin/navbar/navbar.component';
import { FooterAdminComponent } from './admin/footer/footer.component';
import {RouterModule} from '@angular/router';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { SidebarComponent } from './admin/sidebar/sidebar.component';



@NgModule({
  declarations: [
    NavbarPublicComponent,
    FooterPublicComponent,
    NavbarAdminComponent,
    FooterAdminComponent,
    SidebarComponent],
  imports: [
    CommonModule,
    RouterModule,
    FontAwesomeModule
  ],
    exports: [
        NavbarPublicComponent,
        FooterPublicComponent,
        NavbarAdminComponent,
        FooterAdminComponent,
        SidebarComponent
    ]
})
export class ComponentsModule { }
