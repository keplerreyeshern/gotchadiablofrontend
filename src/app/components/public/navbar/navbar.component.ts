import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { faBars, faTimes, faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {ComponentsService} from '../../../services/public/components.service';

@Component({
  selector: 'app-navbar-public',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarPublicComponent implements OnInit {

  //@ts-ignore
  @ViewChild('checkbox') checkbox: ElementRef;
  faTimes = faTimes;
  faBars = faBars;
  faPhoneAlt = faPhoneAlt;

  ngAfterViewInit() {
    // console.log(this.checkbox.nativeElement.value);
  }

  checkValue(){
    // console.log(this.checkbox.nativeElement.checked);
  }

  goto(){
    if(this.checkbox.nativeElement.checked){
      this.checkbox.nativeElement.checked = false;
    }
  }

  constructor(private router: Router,
              private service: ComponentsService) { }

  ngOnInit(): void {
    this.getData();
  }


  getData(){
  }

  clickTel(type: string){
    this.service.setClick(type).subscribe( response => {
      console.log(response);
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
