import { Component, OnInit } from '@angular/core';
import { faFacebook, faTwitter, faYoutube, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faUserAstronaut } from '@fortawesome/free-solid-svg-icons';
import { faUser,  } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-footer-public',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})
export class FooterPublicComponent implements OnInit {

  faInstagram = faInstagram;
  faYoutube = faYoutube;
  faTwitter = faTwitter;
  faFacebook = faFacebook;
  faUser = faUser;
  faUserAstronaut = faUserAstronaut;

  constructor() { }

  ngOnInit(): void {
  }

}
