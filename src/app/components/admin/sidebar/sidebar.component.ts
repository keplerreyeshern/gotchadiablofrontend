import { Component, OnInit } from '@angular/core';
import { faTachometerAlt, faUsers, faClipboardList, faImages, faBoxOpen, faTags, faPlus, faList } from '@fortawesome/free-solid-svg-icons';
import {ComponentsService} from '../../../services/admin/components.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass']
})
export class SidebarComponent implements OnInit {

  faTachometerAlt = faTachometerAlt;
  faUsers = faUsers;
  faClipboardList = faClipboardList;
  faImages = faImages;
  faBoxOpen = faBoxOpen;
  faTags = faTags;
  faPlus = faPlus;
  faList = faList;

  constructor(public serviceMenu: ComponentsService) { }

  ngOnInit(): void {
  }

}
