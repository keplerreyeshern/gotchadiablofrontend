import { Component } from '@angular/core';
import {ComponentsService} from './services/public/components.service';
import {MianService} from './services/public/mian.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'GotchaDiablo';

  constructor(private service: MianService,
              private loading: NgxSpinnerService) {
    // if (!localStorage.getItem('users')){
      this.getData();
    // }
  }


  getData(){
    this.loading.show();
    this.service.getData().subscribe( response => {
      localStorage.setItem('users', JSON.stringify(response.users));
      localStorage.setItem('events', JSON.stringify(response.events));
      localStorage.setItem('packages', JSON.stringify(response.packages));
      localStorage.setItem('images', JSON.stringify(response.images));
      localStorage.setItem('reservations', JSON.stringify(response.reservations));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
      this.loading.hide();
    });
  }
}
