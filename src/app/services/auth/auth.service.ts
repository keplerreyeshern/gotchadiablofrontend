import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = environment.baseUrl;
  public isLoggedIn  = false;

  constructor(private http: HttpClient) {
  }

  postToken(params: any){
    return this.http.post<any>(this.baseUrl + '/oauth/token', params);
  }

  getDataUser(params: any){
    const headers = new HttpHeaders({
      'Authorization': params,
    });
    return this.http.get<any>(this.baseUrl + '/api/user', {headers});
  }

  signIn() {
    sessionStorage.setItem('isLoggedIn', 'true');
    this.isLoggedIn = true;
  }

  getsign() {
    return this.isLoggedIn;
  }

  signOut() {
    sessionStorage.setItem('isLoggedIn', 'false');
    this.isLoggedIn = false;
  }
}
