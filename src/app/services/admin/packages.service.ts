import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PackagesService {

  url = environment.baseUrl + '/api/packages';
  access_token = <string>sessionStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  activePackages(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postPackages(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putPackages(id: number, params: any){
    return this.http.post<any>(this.url + '/' + id, params, {headers: this.headers});
  }

  deletePackages(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
