import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {

  url = environment.baseUrl + '/api/reservations';
  access_token = <string>sessionStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }


  activeReservations(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postReservations(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  statusReservations(id: number, params: any){
    return this.http.put<any>(this.url + '/' + id, params,{headers: this.headers});
  }

  deleteReservations(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
