import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService {

  closeBar = false;

  constructor() { }

  setClose(option: boolean){
    this.closeBar = option;
  }

  getClose(){
    return this.closeBar;
  }
}
