import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url = environment.baseUrl + '/api/users';
  access_token = <string>sessionStorage.getItem('access_token');
  headers:any;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Authorization': this.access_token,
    });
  }

  getUsers(){
    return this.http.get<any>(this.url, {headers: this.headers});
  }

  getUser(email: string){
    return this.http.get<any>(this.url + '/verify/' + email, {headers: this.headers});
  }

  showUser(id: number){
    return this.http.get<any>(this.url + '/' + id, {headers: this.headers});
  }

  activeUsers(id: number){
    return this.http.get<any>(this.url + '/' + id + '/edit', {headers: this.headers});
  }

  postUsers(params: any){
    return this.http.post<any>(this.url, params, {headers: this.headers});
  }

  putUsers(id: any, params: any){
    return this.http.put<any>(this.url + '/' + id, params, {headers: this.headers});
  }

  deleteUsers(id: number){
    return this.http.delete<any>(this.url + '/' + id, {headers: this.headers});
  }
}
