import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ComponentsModule } from '../../components/components.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ComponentsModule,
    NgxSpinnerModule
  ]
})
export class ProfileModule { }
