import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt, faCheck, faBan, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import {Reservation} from '../../../../interfaces/reservation';
import { ReservationsService } from '../../../../services/admin/reservations.service';
import {Package} from '../../../../interfaces/package';
import {User} from '../../../../interfaces/user';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  faBan = faBan;
  faCheck = faCheck;
  faSpinner = faSpinner;
  reservations: Reservation[]=[]
  public page: number | undefined;

  constructor(private titleService: Title,
              private service: ReservationsService,
              private loading: NgxSpinnerService) {
    this.titleService.setTitle("Lista de Reservaciones");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.reservations = JSON.parse(<string>localStorage.getItem('reservations'));
  }

  getPackage(id: number){
    let packages = JSON.parse(<string>localStorage.getItem('packages'));
    // @ts-ignore
    let packagel = <Package>packages.find(item => item.id == id);
    return packagel.title;

  }

  status(id: number, type: number){
    this.loading.show();
    let params = {
      type
    };
    this.service.statusReservations(id, params).subscribe( response => {
      const index = this.reservations.findIndex(item => item.id == response.id);
      this.reservations[index] = response;
      localStorage.setItem('reservations', JSON.stringify(this.reservations));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  getUserTelefone(id: number){
    let users = JSON.parse(<string>localStorage.getItem('users'));
    // @ts-ignore
    let user = <User>users.find(item => item.id == id);
    return user.telephone;
  }

  getUser(id: number){
    let users = JSON.parse(<string>localStorage.getItem('users'));
    // @ts-ignore
    let user = <User>users.find(item => item.id == id);
    return user.name;
  }

  delete(user: number){
    this.loading.show();
    this.service.deleteReservations(user).subscribe( response => {
      const index = this.reservations.findIndex(item => item.id == response.id);
      this.reservations.splice(index, 1);
      localStorage.setItem('reservations', JSON.stringify(this.reservations));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
