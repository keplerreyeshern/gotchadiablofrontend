import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReservationsRoutingModule} from './reservations-routing.module';
import { ReservationsComponent } from './reservations.component';
import { ListComponent } from './list/list.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {NgxPaginationModule} from 'ngx-pagination';



@NgModule({
  declarations: [
    ReservationsComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    ReservationsRoutingModule,
    FontAwesomeModule,
    NgxPaginationModule
  ]
})
export class ReservationsModule { }
