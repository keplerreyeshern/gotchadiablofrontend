import { Component, OnInit } from '@angular/core';
import {Alert} from '../../../../interfaces/alert';
import {environment} from '../../../../../environments/environment';
import {Title} from '@angular/platform-browser';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {EventsService} from '../../../../services/admin/events.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  base_url = environment.baseUrl;
  image:any;
  imageInit:any;
  thumbnail:any;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: EventsService,
              private router: Router) {
    this.titleService.setTitle("Nuevo Usuario");
  }

  ngOnInit(): void {
  }


  submit(form: NgForm){
    this.loading.show();
    let datetime = form.value.date.year+'-'+form.value.date.month+'-'+form.value.date.day+' '+form.value.time.hour+':'+form.value.time.minute+':'+form.value.time.second;
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.name);
    params.append('intro', form.value.intro);
    params.append('date', datetime);
    params.append('description', form.value.description);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.postEvents(params).subscribe(response => {
      let event = response;
      let events = JSON.parse(<string>localStorage.getItem('events'));
      events.push(event);
      localStorage.setItem('events', JSON.stringify(events));
      this.router.navigateByUrl('/admin/events');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      console.log(err.status);
      this.loading.hide();
    });
  }

  closed(){
    this.alert.active = false;
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
