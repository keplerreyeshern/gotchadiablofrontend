import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {Title} from '@angular/platform-browser';
import {NgxSpinnerService} from 'ngx-spinner';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {EventsService} from '../../../../services/admin/events.service';
import {Alert} from '../../../../interfaces/alert';
import {Event} from '../../../../interfaces/event';
import {NgbCalendar, NgbDateAdapter} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  base_url = environment.baseUrl;
  image:any;
  imageInit:any;
  thumbnail:any;
  event: Event=<Event>{};

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: EventsService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private ngbCalendar: NgbCalendar,
              private dateAdapter: NgbDateAdapter<string>) {
    this.activatedRoute.params.subscribe(params => {
      this.getData(params['event']);
    });
  }

  ngOnInit(): void {
  }

  getData(event: string){
    let events = JSON.parse(<string>localStorage.getItem('events'));
    //@ts-ignore
    this.event = events.find(item => item.id == event);
    this.imageInit = this.event.image;
    this.titleService.setTitle("Editar " + this.event.title);
    this.event.time = {
      hour: parseInt(this.event.date.substring(11, 13)),
      minute: parseInt(this.event.date.substring(14, 16)),
      second: parseInt(this.event.date.substring(17, 19)),
    }
    this.event.datet = {
      year: parseInt(this.event.date.substring(0, 4)),
      month: parseInt(this.event.date.substring(5, 7)),
      day: parseInt(this.event.date.substring(8, 10)),
    }
  }


  submit(form: NgForm){
    this.loading.show();
    let datetime = form.value.date.year+'-'+form.value.date.month+'-'+form.value.date.day+' '+form.value.time.hour+':'+form.value.time.minute+':'+form.value.time.second;
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('intro', form.value.intro);
    params.append('date', datetime);
    params.append('description', form.value.description);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.putEvents(this.event.id, params).subscribe(response => {
      let event = response;
      let events = JSON.parse(<string>localStorage.getItem('events'));
      // @ts-ignore
      let index = events.findIndex(item => item.id == event.id)
      events[index] = event;
      localStorage.setItem('events', JSON.stringify(events));
      this.router.navigateByUrl('/admin/events');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      console.log(err.status);
      this.loading.hide();
    });
  }

  closed(){
    this.alert.active = false;
  }

  getImage(e: any){
    this.imageInit = '';
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
