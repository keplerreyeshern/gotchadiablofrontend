import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import {Event} from '../../../../interfaces/event';
import {environment} from '../../../../../environments/environment';
import {Title} from '@angular/platform-browser';
import {NgxSpinnerService} from 'ngx-spinner';
import {EventsService} from '../../../../services/admin/events.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  events: Event[]=[];
  public page: number | undefined;
  base_url = environment.baseUrl;

  constructor(private titleService: Title,
              private service: EventsService,
              private loading: NgxSpinnerService) {
    this.titleService.setTitle("Lista de Eventos");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.events = JSON.parse(<string>localStorage.getItem('events'));
  }

  active(pack: number){
    this.loading.show();
    this.service.activeEvents(pack).subscribe( response => {
      const pack = response;
      const index = this.events.findIndex(item => item.id == pack.id);
      this.events[index].active = response.active;
      localStorage.setItem('events', JSON.stringify(this.events));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(pack: number){
    this.loading.show();
    this.service.deleteEvents(pack).subscribe( response => {
      const index = this.events.findIndex(item => item.id == response.id);
      this.events.splice(index, 1);
      localStorage.setItem('events', JSON.stringify(this.events));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
