import { Component, OnInit } from '@angular/core';
import {Alert} from '../../../../interfaces/alert';
import {environment} from '../../../../../environments/environment';
import {Title} from '@angular/platform-browser';
import {NgxSpinnerService} from 'ngx-spinner';
import {PackagesService} from '../../../../services/admin/packages.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Package} from '../../../../interfaces/package';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  base_url = environment.baseUrl;
  image:any;
  imageInit:any;
  thumbnail:any;
  pack: Package=<Package>{};

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: PackagesService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['pack']);
    });
  }

  ngOnInit(): void {
  }

  getData(pack: string){
    let packages = JSON.parse(<string>localStorage.getItem('packages'));
    //@ts-ignore
    this.pack = packages.find(item => item.id == pack);
    this.imageInit = this.pack.image;
    this.titleService.setTitle("Editar " + this.pack.title);
  }


  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.name);
    params.append('intro', form.value.intro);
    params.append('price', form.value.price);
    params.append('description', form.value.description);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.putPackages(this.pack.id, params).subscribe(response => {
      let pack = response;
      let packages = JSON.parse(<string>localStorage.getItem('packages'));
      // @ts-ignore
      let index = packages.findIndex(item => item.id == pack.id)
      packages[index] = pack;
      localStorage.setItem('packages', JSON.stringify(packages));
      this.router.navigateByUrl('/admin/packages');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      console.log(err.status);
      this.loading.hide();
    });
  }

  closed(){
    this.alert.active = false;
  }

  getImage(e: any){
    this.imageInit = '';
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
