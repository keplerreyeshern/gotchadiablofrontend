import { Component, OnInit } from '@angular/core';
import {Alert} from '../../../../interfaces/alert';
import {NgForm} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {NgxSpinnerService} from 'ngx-spinner';
import {Router} from '@angular/router';
import {PackagesService} from '../../../../services/admin/packages.service';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  base_url = environment.baseUrl;
  image:any;
  imageInit:any;
  thumbnail:any;

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: PackagesService,
              private router: Router) {
    this.titleService.setTitle("Nuevo Usuario");
  }

  ngOnInit(): void {
  }


  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.name);
    params.append('intro', form.value.intro);
    params.append('price', form.value.price);
    params.append('description', form.value.description);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.postPackages(params).subscribe(response => {
      let pack = response;
      let packages = JSON.parse(<string>localStorage.getItem('packages'));
      packages.push(pack);
      localStorage.setItem('packages', JSON.stringify(packages));
      this.router.navigateByUrl('/admin/packages');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      console.log(err.status);
      this.loading.hide();
    });
  }

  closed(){
    this.alert.active = false;
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
