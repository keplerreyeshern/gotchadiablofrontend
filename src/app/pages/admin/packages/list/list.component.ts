import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Package } from '../../../../interfaces/package';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { PackagesService } from '../../../../services/admin/packages.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  packages: Package[]=[];
  public page: number | undefined;
  base_url = environment.baseUrl;

  constructor(private titleService: Title,
              private service: PackagesService,
              private loading: NgxSpinnerService) {
    this.titleService.setTitle("Lista de Paquetes");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.packages = JSON.parse(<string>localStorage.getItem('packages'));
  }

  active(pack: number){
    this.loading.show();
    this.service.activePackages(pack).subscribe( response => {
      const pack = response;
      const index = this.packages.findIndex(item => item.id == pack.id);
      this.packages[index].active = response.active;
      localStorage.setItem('packages', JSON.stringify(this.packages));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(pack: number){
    this.loading.show();
    this.service.deletePackages(pack).subscribe( response => {
      const index = this.packages.findIndex(item => item.id == response.id);
      this.packages.splice(index, 1);
      localStorage.setItem('packages', JSON.stringify(this.packages));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
