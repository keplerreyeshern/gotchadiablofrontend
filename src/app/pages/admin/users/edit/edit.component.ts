import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Title} from '@angular/platform-browser';
import { User } from '../../../../interfaces/user';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {Alert} from '../../../../interfaces/alert';
import {NgxSpinnerService} from 'ngx-spinner';
import {UsersService} from '../../../../services/admin/users.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  //@ts-ignore
  @ViewChild('checkbox') checkbox: ElementRef;
  user: User = <User>{};
  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  };
  passwordV= false;

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title,
              private loading: NgxSpinnerService,
              private service: UsersService,
              private router: Router) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['user']);
    });
  }

  ngOnInit(): void {
  }

  getData(user: string){
    let users = JSON.parse(<string>localStorage.getItem('users'));
    //@ts-ignore
    this.user = users.find(item => item.id == user);
    this.titleService.setTitle("Editar " + this.user.name);
  }

  chagePassword(){
    if (this.checkbox.nativeElement.checked){
      this.passwordV= true;
    } else {
      this.passwordV= false;
    }
  }

  submit(form: NgForm){
    if (this.checkbox.nativeElement.checked){
      if (form.value.password.length < 8){
        this.alert.active = true;
        this.alert.message = 'La contraseña debe tener almenos 8 caracteres';
        setTimeout(() => {
          this.closed()
        }, 7000);
      } else if(form.value.password != form.value.pass) {
        this.alert.active = true;
        this.alert.message = 'Las contraseñas deben coincidir';
        setTimeout(() => {
          this.closed()
        }, 7000);
      } else {
        this. verify(form);
      }
    } else {
      this.verify(form);
    }
  }

  verify(form: NgForm){
    let users = JSON.parse(<string> localStorage.getItem('users'));
    //@ts-ignore
    let user = users.filter(item => item.email == form.value.email);
    let go = false;
    if (user.length > 0) {
      for (let i = 0; user.length > i; i++) {
        if (user[i].email == this.user.email) {
          go = true;
        }
      }
      if (go) {
        this.update(form);
      } else {
        this.alert.active = true;
        this.alert.message = 'El correo ya se encuentra en nuestra base de datos, intenta con otro correo valido';
      }
    } else {
      this.update(form);
    }
  }

  update(form: NgForm){
    this.loading.show();
    let params: any;
    if (this.checkbox.nativeElement.checked){
      params = {
        name: form.value.name,
        email: form.value.email,
        password: form.value.password,
        telephone: form.value.telephone,
        type: 'admin',
        checkbox: 1
      };
    } else {
      params = {
        name: form.value.name,
        email: form.value.email,
        password: form.value.password,
        type: 'admin',
        checkbox: 0
      };
    }
    this.service.putUsers(this.user.id, params).subscribe(response => {
      this.user = response;
      let users = JSON.parse(<string>localStorage.getItem('users'));
      //@ts-ignore
      let index = users.findIndex(item => item.id == this.user.id)
      users[index] = this.user;
      localStorage.setItem('users', JSON.stringify(users));
      this.router.navigateByUrl('/admin/users');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      console.log(err.status);
      this.loading.hide();
    });
  }

  closed(){
    this.alert.active = false;
  }


}
