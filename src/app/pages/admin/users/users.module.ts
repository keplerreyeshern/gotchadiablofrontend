import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UsersRoutingModule} from './users-routing.module';
import { UsersComponent } from './users.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';



@NgModule({
  declarations: [
    UsersComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
    imports: [
        CommonModule,
        UsersRoutingModule,
        FontAwesomeModule,
        FormsModule,
        NgbModule,
        NgxPaginationModule
    ]
})
export class UsersModule { }
