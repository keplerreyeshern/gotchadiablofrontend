import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { User } from '../../../../interfaces/user';
import { Title } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { UsersService } from '../../../../services/admin/users.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  users: User[]=[];
  public page: number | undefined;

  constructor(private titleService: Title,
              private service: UsersService,
              private loading: NgxSpinnerService) {
    this.titleService.setTitle("Lista de Usuarios");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.users = JSON.parse(<string>localStorage.getItem('users'));
  }

  active(user: number){
    this.loading.show();
    this.service.activeUsers(user).subscribe( response => {
      const user = response;
      const index = this.users.findIndex(item => item.id == user.id);
      this.users[index].active = response.active;
      localStorage.setItem('users', JSON.stringify(this.users));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  delete(user: number){
    this.loading.show();
    this.service.deleteUsers(user).subscribe( response => {
      const index = this.users.findIndex(item => item.id == response.id);
      this.users.splice(index, 1);
      localStorage.setItem('users', JSON.stringify(this.users));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
