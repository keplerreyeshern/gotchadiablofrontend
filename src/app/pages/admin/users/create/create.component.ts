import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Title} from "@angular/platform-browser";
import {Alert} from '../../../../interfaces/alert';
import {NgxSpinnerService} from 'ngx-spinner';
import {UsersService} from '../../../../services/admin/users.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.sass']
})
export class CreateComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: UsersService,
              private router: Router) {
    this.titleService.setTitle("Nuevo Usuario");
  }

  ngOnInit(): void {
  }

  submit(form: NgForm){
      if (form.value.password.length < 8){
        this.alert.active = true;
        this.alert.message = 'La contraseña debe tener almenos 8 caracteres';
        setTimeout(() => {
          this.closed()
        }, 7000);
      } else if(form.value.password != form.value.pass) {
        this.alert.active = true;
        this.alert.message = 'Las contraseñas deben coincidir';
        setTimeout(() => {
          this.closed()
        }, 7000);
      } else {
        this. verify(form);
      }
  }

  verify(form: NgForm){
    let users = JSON.parse(<string> localStorage.getItem('users'));
    //@ts-ignore
    let user = users.filter(item => item.email == form.value.email);
    if (user.length > 0) {
      this.alert.active = true;
      this.alert.message = 'El correo ya se encuentra en nuestra base de datos, intenta con otro correo valido';
    } else {
      this.store(form);
    }
  }

  store(form: NgForm){
    this.loading.show();
    let params: any;
    params = {
      name: form.value.name,
      email: form.value.email,
      password: form.value.password,
      telephone: form.value.telephone,
      type: 'admin',
    };
    this.service.postUsers(params).subscribe(response => {
      let user = response;
      let users = JSON.parse(<string>localStorage.getItem('users'));
      users.push(user);
      localStorage.setItem('users', JSON.stringify(users));
      this.router.navigateByUrl('/admin/users');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      console.log(err.status);
      this.loading.hide();
    });
  }

  closed(){
    this.alert.active = false;
  }

}
