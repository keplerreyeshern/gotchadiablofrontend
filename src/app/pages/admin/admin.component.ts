import { HostListener } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ComponentsService } from '../../services/admin/components.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if (window.innerWidth < 769) {
      this.serviceMenu.setClose(true);
    } else {
      this.serviceMenu.setClose(false);
    }
  }


  constructor(public serviceMenu: ComponentsService,
              private titleService: Title) {
    this.titleService.setTitle('Administrador');
  }

  ngOnInit(): void {
  }

}
