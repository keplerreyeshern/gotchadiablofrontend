import { Component, OnInit } from '@angular/core';
import { faTable, faPlus, faPowerOff, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import {Image} from '../../../../interfaces/image';
import {environment} from '../../../../../environments/environment';
import {Title} from '@angular/platform-browser';
import {NgxSpinnerService} from 'ngx-spinner';
import {GalleryService} from '../../../../services/admin/gallery.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  faTable = faTable;
  faPlus = faPlus;
  faPowerOff = faPowerOff;
  faTrashAlt = faTrashAlt;
  images: Image[]=[];
  public page: number | undefined;
  base_url = environment.baseUrl;

  constructor(private titleService: Title,
              private service: GalleryService,
              private loading: NgxSpinnerService) {
    this.titleService.setTitle("Galeria");
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.images = JSON.parse(<string>localStorage.getItem('images'));
  }

  delete(image: number){
    this.loading.show();
    this.service.deleteGallery(image).subscribe( response => {
      const index = this.images.findIndex(item => item.id == image);
      this.images.splice(index, 1);
      localStorage.setItem('images', JSON.stringify(this.images));
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
