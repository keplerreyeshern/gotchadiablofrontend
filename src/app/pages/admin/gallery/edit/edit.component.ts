import { Component, OnInit } from '@angular/core';
import {Alert} from '../../../../interfaces/alert';
import {Image} from '../../../../interfaces/image';
import {environment} from '../../../../../environments/environment';
import {Title} from '@angular/platform-browser';
import {NgxSpinnerService} from 'ngx-spinner';
import {GalleryService} from '../../../../services/admin/gallery.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {NgbCalendar, NgbDateAdapter} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.sass']
})
export class EditComponent implements OnInit {

  alert: Alert = {
    type: 'danger',
    message: '',
    active: false
  }
  base_url = environment.baseUrl;
  image:any;
  imageInit:any;
  thumbnail:any;
  imageG: Image=<Image>{};

  constructor(private titleService: Title,
              private loading: NgxSpinnerService,
              private service: GalleryService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(params => {
      this.getData(params['image']);
    });
  }

  ngOnInit(): void {
  }

  getData(image: string){
    let images = JSON.parse(<string>localStorage.getItem('images'));
    //@ts-ignore
    this.imageG = images.find(item => item.id == image);
    this.imageInit = this.imageG.image;
    this.titleService.setTitle("Editar " + this.imageG.title);
  }


  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('Content-Type', 'multipart/form-data');
    params.append('title', form.value.title);
    params.append('description', form.value.description);
    if (this.image){
      params.append('image', this.image);
    }
    this.service.putGallery(this.imageG.id, params).subscribe(response => {
      let imageG = response;
      let gallery = JSON.parse(<string>localStorage.getItem('images'));
      // @ts-ignore
      let index = gallery.findIndex(item => item.id == imageG.id)
      gallery[index] = imageG;
      localStorage.setItem('images', JSON.stringify(gallery));
      this.router.navigateByUrl('/admin/gallery');
      this.loading.hide();
    }, err => {
      if (err.status == 500) {
        this.alert.active = true;
        this.alert.message = 'Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador';
      } else {
        this.alert.active = true;
        this.alert.message = 'Se detecto un error comunicate con el administrador';
      }
      console.log(err.status);
      this.loading.hide();
    });
  }

  closed(){
    this.alert.active = false;
  }

  getImage(e: any){
    let file = e.target.files[0];
    this.image = file;
    this.uploadImage(file);
  }

  uploadImage(file: any){
    let reader = new FileReader();
    reader.onload = (e) => {
      //@ts-ignore
      this.thumbnail = e.target.result;
    }

    reader.readAsDataURL(file);
  }

}
