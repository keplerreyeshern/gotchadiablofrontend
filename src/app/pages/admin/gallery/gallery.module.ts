import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './gallery.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import { RouterModule } from '@angular/router';
import { EditComponent } from './edit/edit.component';
import { GalleryRoutingModule } from './gallery-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    GalleryComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    GalleryRoutingModule,
    NgxPaginationModule,
    FontAwesomeModule,
    NgbModule,
    FormsModule
  ]
})
export class GalleryModule { }
