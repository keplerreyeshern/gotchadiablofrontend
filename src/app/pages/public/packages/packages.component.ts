import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {MianService} from '../../../services/public/mian.service';
import {Package} from '../../../interfaces/package';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.sass']
})
export class PackagesComponent implements OnInit {

  base_url = environment.baseUrl;
  packages: Package[]=[];

  constructor(private service: MianService,
              private titleService: Title) {
    this.titleService.setTitle("Paquetes");
  }

  ngOnInit(): void {
    if (!localStorage.getItem('packages')){
      this.getData();
    } else {
      this.packages = JSON.parse(<string>localStorage.getItem('packages'));
    }
  }

  getData(){
    this.service.getData().subscribe( response => {
      this.packages = response.packages;
      // this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
      // this.loading.hide();
    });
  }

}
