import { Component, OnInit } from '@angular/core';
import {Package} from '../../../../interfaces/package';
import {ActivatedRoute} from '@angular/router';
import {environment} from '../../../../../environments/environment';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.sass']
})
export class ShowPackagesComponent implements OnInit {

  package: Package = <Package> {};
  base_url = environment.baseUrl;

  constructor(private activatedRoute: ActivatedRoute,
              private titleService: Title) {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['package']);
    });
  }

  ngOnInit(): void {

  }

  getData(slug: string){
    let packages = JSON.parse(<string>localStorage.getItem('packages'));
    //@ts-ignore
    this.package = packages.find(item => item.slug == slug);
    this.titleService.setTitle("Detalle del paquete " + this.package.title);
  }

}
