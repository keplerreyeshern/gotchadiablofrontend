import {Component, OnInit, ViewChild} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {MianService} from '../../../services/public/mian.service';
import {Title} from '@angular/platform-browser';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  //@ts-ignore
  @ViewChild('carousel', {static : true}) carousel: NgbCarousel;
  base_url = environment.baseUrl;
  packagesAll: any[]=[];
  packages: any[]=[];
  images: any[]=[
    {img: 'assets/images/slider/slider.png'},
    {img: 'assets/images/slider/slider2.png'},
    {img: 'assets/images/slider/slider3.png'},
  ];

  constructor(private service: MianService,
              private titleService: Title) {
    this.titleService.setTitle("Gotcha Diablo");
  }

  ngOnInit(): void {
    if (!localStorage.getItem('packages')){
      this.getData();
      this.setPackage();
    } else {
      this.packagesAll = JSON.parse(<string>localStorage.getItem('packages'));
      this.setPackage();
    }
  }

  setPackage(){
    for (let i=0; i<3; i++){
      this.packages.push(this.packagesAll[i]);
    }
  }

  getData(){
    this.service.getData().subscribe( response => {
      this.packagesAll = response.packages;
      // this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
      // this.loading.hide();
    });
  }


}
