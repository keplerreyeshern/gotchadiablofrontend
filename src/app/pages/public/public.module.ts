import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PublicRoutingModule} from './public-routing.module';
import {ComponentsModule} from '../../components/components.module';
import {RecaptchaModule} from 'ng-recaptcha';
import {NgxSpinnerModule} from 'ngx-spinner';
import { ReservationsComponent } from './reservations/reservations.component';
import { PackagesComponent } from './packages/packages.component';
import { EventsComponent } from './events/events.component';
import { GalleryComponent } from './gallery/gallery.component';
import {FormsModule} from '@angular/forms';
import {ContactComponent} from './contact/contact.component';
import { PrivacyComponent } from './privacy/privacy.component';
import {MainComponent} from './main/main.component';
import {TruncatePipe} from '../../pipes/truncate.pipe';
import { ShowPackagesComponent } from './packages/show/show.component';
import { ShowEventsComponent } from './events/show/show.component';
import {IvyGalleryModule} from 'angular-gallery';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';



@NgModule({
    declarations: [
        ReservationsComponent,
        MainComponent,
        PackagesComponent,
        EventsComponent,
        GalleryComponent,
        ContactComponent,
        PrivacyComponent,
        TruncatePipe,
        ShowPackagesComponent,
        ShowEventsComponent
    ],
    exports: [
        TruncatePipe
    ],
    imports: [
        CommonModule,
        PublicRoutingModule,
        ComponentsModule,
        NgxSpinnerModule,
        RecaptchaModule,
        FormsModule,
        IvyGalleryModule,
        NgbModule
    ]
})
export class PublicModule { }
