import { Component, OnInit } from '@angular/core';
import { Event } from '../../../../interfaces/event';
import { environment } from '../../../../../environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.sass']
})
export class ShowEventsComponent implements OnInit {

  //@ts-ignore
  event: Event;
  base_url = environment.baseUrl;

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.getData(params['event']);
    });

  }

  getData(slug: string){
    let events = JSON.parse(<string>localStorage.getItem('events'));
    //@ts-ignore
    this.event = events.find(item => item.slug == slug);
  }

}
