import { Component, OnInit } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {MianService} from '../../../services/public/mian.service';
import { Event } from '../../../interfaces/event';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.sass']
})
export class EventsComponent implements OnInit {

  base_url = environment.baseUrl;
  events: Event[]=[];

  constructor(private service: MianService,
              private titleService: Title) {
    this.titleService.setTitle("Eventos");
  }

  ngOnInit(): void {
    if (!localStorage.getItem('events')){
      this.getData();
    } else {
      this.events = JSON.parse(<string>localStorage.getItem('events'));
    }
  }

  getData(){
    this.service.getData().subscribe( response => {
      this.events = response.events;
      // this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
      // this.loading.hide();
    });
  }

}
