import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { MianService } from '../../../services/public/mian.service';
import { Image } from '../../../interfaces/image';
import {Gallery} from 'angular-gallery';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.sass']
})
export class GalleryComponent implements OnInit {

  images: any[]=[];
  imagesAll: any[]=[];
  base_url = environment.baseUrl;

  constructor(private service: MianService,
              private gallery: Gallery,
              private titleService: Title) {
    this.titleService.setTitle("Galeria");
  }

  ngOnInit(): void {
    if (!localStorage.getItem('images')){
      this.getData();
    } else {
      this.images = JSON.parse(<string>localStorage.getItem('images'));
    }
  }

  showGallery(index: number) {
    let images:any[]=[];
    for(let i=0; i<this.images.length; i++){
      images.push({path: this.base_url + this.images[i].image})
    }
    let prop = {
      images: images,
      index
    };
    this.gallery.load(prop);
  }

  getData(){
    this.service.getData().subscribe( response => {
      this.images = response.images;
      // this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
      // this.loading.hide();
    });
  }

}
