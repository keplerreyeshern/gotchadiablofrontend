import { Component, OnInit } from '@angular/core';
import {ComponentsService} from '../../services/public/components.service';
import {MianService} from '../../services/public/mian.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {filter} from 'rxjs/operators';
import {NavigationEnd, Router} from '@angular/router';
// @ts-ignore
declare var gtag;


@Component({
  selector: 'app-public',
  templateUrl: './public.component.html',
  styleUrls: ['./public.component.sass']
})
export class PublicComponent implements OnInit {

  constructor(private serviceComp: ComponentsService,
              private router: Router) {
    const navEndEvents$ = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd)
      );

    // @ts-ignore
    navEndEvents$.subscribe((event: NavigationEnd) => {
      gtag('config', 'G-Y2VV5XJB4', {
        'page_path': event.urlAfterRedirects
      });
    });
    sessionStorage.setItem('language', 'es');
  }

  ngOnInit(): void {
  }


  clickWP(type: string){
    this.serviceComp.setClick(type).subscribe( response => {
      console.log(response);
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else {
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

}
