import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PublicComponent} from './public.component';
import {MainComponent} from './main/main.component';
import {ContactComponent} from './contact/contact.component';
import {GalleryComponent} from './gallery/gallery.component';
import {EventsComponent} from './events/events.component';
import {PackagesComponent} from './packages/packages.component';
import {ReservationsComponent} from './reservations/reservations.component';
import {PrivacyComponent} from './privacy/privacy.component';
import {ShowPackagesComponent} from './packages/show/show.component';
import {ShowEventsComponent} from './events/show/show.component';
import {AuthGuard} from '../../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        component: MainComponent
      },
      // {
      //   path: 'reservacion',
      //   component: ReservationsComponent,
      //   canActivate: [AuthGuard]
      // },
      {
        path: 'paquetes',
        component: PackagesComponent
      },
      {
        path: 'paquetes/:package',
        component: ShowPackagesComponent
      },
      {
        path: 'eventos',
        component: EventsComponent
      },
      {
        path: 'eventos/:event',
        component: ShowEventsComponent
      },
      {
        path: 'galeria',
        component: GalleryComponent
      },
      {
        path: 'contacto',
        component: ContactComponent
      },
      {
        path: 'aviso-privacidad',
        component: PrivacyComponent
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule { }
