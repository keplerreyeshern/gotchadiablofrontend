import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.sass']
})
export class ReservationsComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("Realiza tu Reservación");
  }

  ngOnInit(): void {
  }

}
