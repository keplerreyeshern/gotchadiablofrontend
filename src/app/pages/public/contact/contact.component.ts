import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ContactService} from '../../../services/public/contact.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  alert = false;
  alertM =false;
  recaptcha = false;

  constructor(private service: ContactService,
              private loading: NgxSpinnerService,
              private titleService: Title) {
    this.titleService.setTitle("Contacto");
  }

  ngOnInit(): void {

  }

  submit(form: NgForm){
    if(!this.recaptcha){
      this.alert = true;
    } else {
      this.loading.show();
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('name', form.value.name);
      params.append('telephone', form.value.telephone);
      params.append('email', form.value.email);
      params.append('message', form.value.message);
      this.service.contact(params).subscribe( response => {
        console.log(response);
        this.alert = true;
        form.resetForm();
        this.loading.hide();
      }, err => {;
        if(err.status == 500){
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        } else {
          alert('se detecto un error comunicate con el administrador');
        }
        this.loading.hide();
      });
    }
  }

  resolved(captchaResponse: string) {
    console.log(`Resolved captcha with response: ${captchaResponse}`);
    if(captchaResponse != null){
      this.recaptcha = true;
    } else {
      this.recaptcha = false;
    }
  }

}
