import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.sass']
})
export class PrivacyComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("Aviso de Privacidad");
  }

  ngOnInit(): void {
  }

}
