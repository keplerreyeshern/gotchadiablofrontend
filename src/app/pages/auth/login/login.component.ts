import { Component, OnInit } from '@angular/core';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {AuthService} from '../../../services/auth/auth.service';
import {NgForm} from '@angular/forms';
import {Title} from '@angular/platform-browser';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  alert= {
    type: 'danger',
    message: '',
  };
  alertB = false;
  storage = '';
  url = '';
  access_token = '';
  type = 'password';
  reservationBack = false;


  constructor(private spinnerService: NgxSpinnerService,
              private authService: AuthService,
              private router: Router,
              private titleService: Title) {
    this.titleService.setTitle("¡Entrar!");
  }

  ngOnInit(): void {
  }

  closeAlert() {
    this.alertB = false;
  }

  submit(form: NgForm){
    this.spinnerService.show();
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'UoGHWTuUaiZZIclE0qHpKAbN13tIf9YwPyd3TucQ',
      username: form.value.email,
      password: form.value.password,
    };
    this.authService.postToken(params).subscribe( response => {
      // console.log(response);
      sessionStorage.setItem('token', JSON.stringify(response));
      this.storage = <string> sessionStorage.getItem('token');
      let start = this.storage.indexOf('access_token', 0);
      let substr = this.storage.substring(start);
      start = substr.indexOf(':', 0) + 2;
      substr = substr.substring(start);
      const end = substr.indexOf('"', 0);
      this.access_token = substr.substring(0, end );
      sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
      // console.log(sessionStorage.getItem('access_token'));
      this.access(form.value.email);
    }, err => {
      if(err.status == 400){
        this.spinnerService.hide();
        this.alert.message = 'Las credenciales no coinciden en la base de datos. Verifica y vuelve a intentar';
        this.alertB = true;
        this.setTime();
      } else if(err.status == 500){
        this.spinnerService.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 404){
        this.spinnerService.hide();
        alert('¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador');
      } else {
        this.spinnerService.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  access(email: string){
    let users = JSON.parse(<string>localStorage.getItem('users'));
    // @ts-ignore
    let user  = users.find(item => item.email == email)
    if (user.active){
      this.authService.signIn();
      localStorage.setItem('user', JSON.stringify(user));
        if(user.profile == 'admin'){
            this.router.navigate(['/admin']);
        } else {
            this.router.navigate(['/perfil']);
        }
    } else {
      sessionStorage.clear();
      this.alert.message = 'Tu usuario tiene bloqueado el acceso comunicate con el administrador';
      this.alertB = true;
      this.setTime();
      this.authService.signOut();
    }
    this.spinnerService.hide();
  }

  setTime(){
    setTimeout(() => {
      this.alertB = false;
    },10000);
  }

}
