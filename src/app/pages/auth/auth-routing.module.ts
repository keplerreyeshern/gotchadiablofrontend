import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthComponent} from './auth.component';
import {LoginComponent} from './login/login.component';
import {EmailComponent} from './email/email.component';
import {FindComponent} from './find/find.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'in',
        component: LoginComponent
      },
      {
        path: 'password/create',
        component: EmailComponent
      },
      {
        path: 'password/reset/:token',
        component: FindComponent
      },
      {
        path: '',
        redirectTo: 'in',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule { }
