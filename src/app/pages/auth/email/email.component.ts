import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { PasswordService } from '../../../services/auth/password.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.sass']
})
export class EmailComponent implements OnInit {

  alert= {
    type: 'success',
    message: '',
  };
  message = '';
  alertB = false;

  constructor(private loading: NgxSpinnerService,
              private service: PasswordService,
              private titleService: Title) {
    this.titleService.setTitle("Recupera tu contraseña");
  }

  ngOnInit(): void {
  }

  closeAlert() {
    this.alertB = false;
  }

  submit(form: NgForm){
    this.loading.show();
    let params = new FormData();
    params.append('email', form.value.email);
    this.service.createPassword(params).subscribe( response => {
      this.alert.message = response.message;
      this.alertB = true;
      form.resetForm();
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        this.loading.hide();
      } else if(err.status == 404){
        this.alert.message = err.error.message;
        this.alertB = true;
        form.resetForm();
        this.loading.hide();
      } else {
        alert('se detecto un error comunicate con el administrador');
        this.loading.hide();
      }
    });
  }

}
