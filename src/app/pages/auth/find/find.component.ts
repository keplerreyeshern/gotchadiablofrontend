import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordService } from '../../../services/auth/password.service';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../../services/auth/auth.service';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.sass']
})
export class FindComponent implements OnInit {

  message = '';
  token = '';
  data: any;
  alert = false;
  disabled = false;
  messagePassword = '';
  alertPassword = false;
  user:any;
  storage = '';
  url = '';
  access_token = '';
  password = '';

  constructor(private loading: NgxSpinnerService,
              private service: PasswordService,
              private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private titleService: Title) {
    this.titleService.setTitle("Cambia tu contraseña");
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe( params => {
      this.tokenFind(params['token']);
      this.token = params['token'];
    });
  }

  tokenFind(token: string){
    this.loading.show();
    this.service.tokenFind(token).subscribe( response => {
      this.data = response;
      // console.log(response);
      this.loading.hide();
    }, err => {
      if(err.status == 500){
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
        this.loading.hide();
      } else if(err.status == 404){
        this.message = err.error.message;
        this.alert = true;
        this.disabled = true;
        this.loading.hide();
      } else {
        alert('se detecto un error comunicate con el administrador');
        this.loading.hide();
      }
    });
  }

  submit(form: NgForm){
    if(form.value.password.length < 8){
      this.alertPassword = true;
      this.messagePassword = 'La contraseña debe tener minimo 8 caracteres';
    } else if (form.value.password != form.value.repeatPassword ){
      this.alertPassword = true;
      this.messagePassword = 'Las contraseñas debe coincidir';
    } else {
      this.password = form.value.password;
      this.loading.show();
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('email', this.data.email);
      params.append('token', this.data.token);
      params.append('password', form.value.password);
      this.service.resetPassword(params).subscribe(response => {
        this.user = response;
        this.login();
        // console.log(response);
      }, err => {
        if (err.status == 500) {
          alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
          this.loading.hide();
        } else if (err.status == 404) {
          this.message = err.error.message;
          this.alert = true;
          form.resetForm();
          this.loading.hide();
        } else {
          alert('se detecto un error comunicate con el administrador');
          this.loading.hide();
        }
      });
    }
  }


  login(){
    const params = {
      grant_type: 'password',
      client_id: '2',
      client_secret: 'UoGHWTuUaiZZIclE0qHpKAbN13tIf9YwPyd3TucQ',
      username: this.user.email,
      password: this.password,
    };
    this.authService.postToken(params).subscribe( response => {
      // console.log(response);
      sessionStorage.setItem('token', JSON.stringify(response));
      this.storage = <string> sessionStorage.getItem('token');
      let start = this.storage.indexOf('access_token', 0);
      let substr = this.storage.substring(start);
      start = substr.indexOf(':', 0) + 2;
      substr = substr.substring(start);
      const end = substr.indexOf('"', 0);
      this.access_token = substr.substring(0, end );
      sessionStorage.setItem('access_token', 'Bearer ' + this.access_token);
      // console.log(sessionStorage.getItem('access_token'));
      this.access();
    }, err => {
      if(err.status == 400){
        this.loading.hide();
        this.alert = true;
      } else if(err.status == 500){
        this.loading.hide();
        alert('Se encontro error con el servidor, intenta mas tarde, si el error persiste comunicate con el administrador');
      } else if(err.status == 404){
        this.loading.hide();
        alert('¡Error Grave!, A la pagina que se intenta accesar no existe, comuniquese de inmediato con el administrador');
      } else {
        this.loading.hide();
        alert('se detecto un error comunicate con el administrador');
      }
    });
  }

  access(){
    let users = JSON.parse(<string>localStorage.getItem('users'));
    // @ts-ignore
    let user  = users.find(item => item.email == this.user.email)
    if (user.active){
      this.authService.signIn();
      if(user.profile == 'admin'){
        this.router.navigate(['/admin']);
      } else {
        this.router.navigate(['/perfil']);
      }
    } else {
      sessionStorage.clear();
      this.authService.signOut();
    }
    this.loading.hide();
  }

}
