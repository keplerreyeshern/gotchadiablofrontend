import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { ComponentsModule } from '../../components/components.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EmailComponent } from './email/email.component';
import { FindComponent } from './find/find.component';



@NgModule({
  declarations: [
    LoginComponent,
    EmailComponent,
    FindComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ComponentsModule,
    NgxSpinnerModule,
    FormsModule,
    NgbModule,
  ]
})
export class AuthModule { }
